#include <Octabio.h>
#include <Motor.h>

const double factor = 500.0 / 15.45;
unsigned char actionList[1000];
unsigned int maxActions = 1000;
unsigned int index = 0;

void avanzar(double distancia)
{
  //Serial.println("Avanza!");
  setMotor(1, 255);  // izq
  setMotor(2, -240); // ders
  delay(factor * distancia);
  setMotor(1, 0);
  setMotor(2, 0);
  delay(400);
}

void retroceder(double distancia)
{
  //Serial.println("Retrocede!");
  setMotor(1, -255); // izq
  setMotor(2, 240);  // ders
  delay(factor * distancia);
  setMotor(1, 0);
  setMotor(2, 0);
  delay(400);
}

void girarDerecha()
{
  //Serial.println("Gira derecha!");
  setMotor(1, 255); // izq
  setMotor(2, 255); // ders
  delay(220);
  setMotor(1, 0);
  setMotor(2, 0);
  delay(400);
}

void girarIzquierda()
{
  //Serial.println("Gira izquierda!");
  setMotor(1, -255); // izq
  setMotor(2, -255); // ders
  delay(215);
  setMotor(1, 0);
  setMotor(2, 0);
  delay(400);
}

void doAction(int actionIndex, int value = 15)
{
  if (actionIndex == 0)
  {
    avanzar(value);
  }
  if (actionIndex == 1)
  {
    retroceder(value);
  }
  if (actionIndex == 2)
  {
    girarDerecha();
  }
  if (actionIndex == 3)
  {
    girarIzquierda();
  }
}

void setup()
{
  // put your setup code here, to run once:
  pinMode(PIN_A, INPUT);
  pinMode(PIN_B, INPUT);
  pinMode(PIN_E, INPUT);
  pinMode(PIN_F, INPUT);
  setUpMotor(1);
  setUpMotor(2);

  //Serial.begin(9600);

  delay(2000);

  //Serial.println("Entering setup mode");
  //Serial.print("ActionList size:");
  //Serial.println(index);
}

// unsigned long timestamp = 0;
int state = 0;
bool aState = false;
bool bState = false;
bool eState = false;
bool fState = false;


void loop()
{
  // if (millis() - timestamp > 5000)
  // {
  //   timestamp = millis();
  // }

  if (state == 0)
  {
    while(digitalRead(PIN_A)){
      delay(50);
      aState = true;
      if(digitalRead(PIN_B)){
        state=1;
        aState=false;
        return;
      }
    }

    if (aState)
    {
      aState=false;
      // save action
      actionList[index] = 0;
      index++;
      // Serial.println("Avanzar guardado!");
    }

    while(digitalRead(PIN_B)){
      delay(50);
      bState = true;
      if(digitalRead(PIN_A)){ 
        state = 1;
        bState = false;
        return;
      };
    }

    if (bState)
    {
      bState = false;
      // save action
      actionList[index] = 1;
      index++;
      // Serial.println("Retroceder guardado!");
    }

    while (digitalRead(PIN_E))
    {
      delay(50);
      eState = true;
    }
    

    if (eState)
    {
      eState = false;
      // save action
      // Serial.println("Derecha guardado!");
      actionList[index] = 2;
      index++;
    }

    while (digitalRead(PIN_F))
    {
      delay(50);
      fState = true;
    }

    if (fState)
    {
      fState = false;
      actionList[index] = 3;
      index++;
      // Serial.println("Izquierda guardado!");
      // save action
    }

    if (
        aState &&
        bState)
    {
      state = 1;
    }
  }

  if (state == 1)
  {
    if (
        !digitalRead(PIN_A) &&
        !digitalRead(PIN_B))
    {
      // do saved actions
      for (unsigned int i = 0; i < index; i++)
      {
        int action = actionList[i];
        doAction(action);
      }
      state = 0;
      index = 0;
    }
  }

  aState = digitalRead(PIN_A);
  bState = digitalRead(PIN_B);
  eState = digitalRead(PIN_E);
  fState = digitalRead(PIN_F);
}